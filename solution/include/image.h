#ifndef INC_3LAB_IMAGE_H
#define INC_3LAB_IMAGE_H

#include <stdint.h>
struct image {
    uint64_t width, height;
    struct pixel* data;
};
#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)
struct pixel* malloc_image(uint64_t width, uint64_t height);

void destroy_image(struct image* image);
#endif
