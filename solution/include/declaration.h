#ifndef INC_3LAB_BMP_H
#include "image.h"
#include <stdint.h>
#include <stdio.h>
#define PIXEL_SIZE 3
#pragma pack(push, 1)

//structures

struct bmp_header
{
    uint16_t bf_type;
    uint32_t bf_file_Size;
    uint32_t bf_reserved;
    uint32_t b_off_Bits;
    uint32_t bi_size;
    uint32_t bi_width;
    uint32_t bi_height;
    uint16_t bi_planes;
    uint16_t bi_bit_count;
    uint32_t bi_compression;
    uint32_t bi_size_image;
    uint32_t bi_x_pels_per_meter;
    uint32_t bi_y_pels_per_meter;
    uint32_t bi_clr_used;
    uint32_t bi_clr_important;
};
#pragma pack(pop)

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR

};


//Methods

enum read_status read_bmp_and_pixels(FILE* file, struct image* image);

enum write_status write_bmp(FILE* file, struct image const* image );

#endif
