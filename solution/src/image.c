#include "malloc.h"
#include <image.h>
#include <stdint.h>

void destroy_image(struct image* const image) {
    free(image->data);

}

struct pixel* malloc_image(uint64_t const width, uint64_t const height) {
    return (struct pixel*) malloc(sizeof(struct pixel)*width*height);
}
