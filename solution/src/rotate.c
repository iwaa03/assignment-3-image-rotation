#include <image.h>
#include <stdint.h>

void transform_image(struct image const new_image, struct image const source_image) {
    uint32_t current_pixel = 0;
    for(uint32_t i = 0; i <= source_image.width - 1; i++) {
        for(uint32_t j = source_image.height - 1; j > 0; j--) {
            new_image.data[current_pixel] = source_image.data[i + j * source_image.width];
            current_pixel++;
        }
        new_image.data[current_pixel] = source_image.data[i];
        current_pixel++;
    }
}

struct image make_result(struct image new_image, struct image const source_image) {
    new_image.height = source_image.width;
    new_image.width = source_image.height;
    return new_image;
}

struct image rotate_image(struct image const source_image ) {
    struct image new_image = {0};
    new_image.data = malloc_image(source_image.width, source_image.height);
    if (new_image.data == 0) return new_image;
    transform_image(new_image, source_image);
    return make_result(new_image, source_image);
}
