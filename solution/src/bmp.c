#include "declaration.h"

#define BISIZE 40
#define BFREZERVED 0
#define HEADER_SIZE sizeof(struct bmp_header)
#define OFFSET 54
#define BITS 24
#define PLANES 1
#define COMPRESSION 0
#define PIXELS_PER_METER 2835
#define COLORS_IMPORTANT 0
#define BFTYPE 19778
#define COLORS_USED 0

uint32_t extra_bytes(uint32_t width) {
    return 4 - width * PIXEL_SIZE % 4;
}

size_t get_size(struct image const* image) {
    return (image->width) * (image->height) + extra_bytes(image->width) * (image->height);
}

struct bmp_header make_header(struct image const* image) {
    struct bmp_header header = {0};
    header.bf_type = BFTYPE;
    header.bf_reserved = BFREZERVED;
    header.b_off_Bits = OFFSET;
    header.bi_size = BISIZE;
    header.bi_width = image->width;
    header.bi_height = image->height;
    header.bi_planes = PLANES;
    header.bi_bit_count = BITS;
    header.bi_compression = COMPRESSION;
    header.bi_size_image = 0;
    header.bi_x_pels_per_meter = PIXELS_PER_METER;
    header.bi_y_pels_per_meter = PIXELS_PER_METER;
    header.bi_clr_used = COLORS_USED;
    header.bi_clr_important = COLORS_IMPORTANT;
    header.bi_size_image = get_size(image);
    header.bf_file_Size = header.bi_size_image + HEADER_SIZE;
    return header;
}

enum read_status read_image(FILE* file, struct image* const image, struct bmp_header const header) {
    uint32_t width = header.bi_width;
    uint32_t height = header.bi_height;
    struct pixel *pixels_begging = malloc_image(width, height);
    if (pixels_begging == 0) {
        return READ_INVALID_BITS;
    }
    for (size_t i = 0; i < height; i++) {
        size_t readed_width = fread(pixels_begging + i * width, sizeof(struct pixel), width, file);
        if (readed_width != width) return READ_INVALID_BITS;
        uint32_t mod = extra_bytes(width);
        if (fseek(file, mod, SEEK_CUR) != 0) return READ_INVALID_BITS;
    }
    image->data = pixels_begging;
    image->height = height;
    image->width = width;
    return READ_OK;
}

size_t write_header(FILE* file, struct image const* image) {
    struct bmp_header header = make_header(image);
    return fwrite(&header, HEADER_SIZE, 1, file);
}

enum write_status write_image(FILE* file, struct image const* image) {
    for (int i = 0; i < image->height; i++) {
        size_t writed_width = fwrite(image->data + i * (image->width), PIXEL_SIZE, image->width, file);
        if (writed_width < image->width) return WRITE_ERROR;
        size_t mod = extra_bytes(image->width);
        size_t writed_extra  = fwrite(image->data, 1, mod, file);
        if (writed_extra != mod) return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum read_status read_bmp_and_pixels(FILE* file, struct image* const image) {
    struct bmp_header header = {0};
    size_t count = fread(&header, sizeof(struct bmp_header), 1, file);
    if (count != 1) return READ_INVALID_HEADER;
    if (header.bf_type != BFTYPE) return READ_INVALID_SIGNATURE;
    if (header.bi_bit_count != BITS) return READ_INVALID_BITS;
    return read_image(file, image, header);
}

enum write_status write_bmp(FILE* const file, struct image const* image) {
    if (write_header(file, image) != 1) return WRITE_ERROR;
    return write_image(file, image);
}
