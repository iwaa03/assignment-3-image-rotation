#include "declaration.h"
#include "rotate.h"
#include <stdbool.h>
#include <stdio.h>

 int exit_program(char *message, struct image image, struct image new_image, FILE* const read_file, FILE* const write_file, bool const is_error) {
    destroy_image(&image);
    destroy_image(&new_image);
    if (read_file != NULL) {
        if (fclose(read_file) != 0) {
            fprintf(stderr, "failed to close reading file");
            return 1;
        }
    }
    if (write_file != NULL) {
        if (fclose(write_file) != 0) {
            fprintf(stderr, "failed to close writing file");
            return 1;
        }
    }
    if (is_error) {
        fprintf(stderr, "%s", message);
        return 1;
    }
    printf("%s", message);
    return 0;
}

int main(int arg, char const **mass) {
    struct image new_image = {0};
    struct image image = {0};
    if (arg < 3) return exit_program("There must be 3 parameters", image, new_image, NULL, NULL, true);
    FILE* read_file = fopen(mass[1], "rb");
    FILE* write_file = fopen(mass[2], "wb");
    if (read_file == NULL) return exit_program("There is no reading file", image, new_image, read_file, write_file, true);
    if (write_file == NULL) return exit_program("There is no writing file", image, new_image, read_file, write_file, true);
    if (read_bmp_and_pixels(read_file, &image) != READ_OK) return exit_program("something went wrong", image, new_image, read_file, write_file, true);
    new_image = rotate_image(image);
    if (new_image.data == 0) return exit_program("failed to allocate memory", image, new_image, read_file, write_file, true);
    if (write_bmp(write_file, &new_image) == WRITE_ERROR) return exit_program("Something went wrong", image, new_image, read_file, write_file, true);
    return exit_program("Rotation successful", image, new_image, read_file, write_file, false);
}
